package trace;

import club.*;
import java.util.*;
import static java.util.Calendar.*;

public class Trace {
    
    public static void main(String[] args) {
        
        Calendar cal = Calendar.getInstance();
        cal.set(1, Calendar.JANUARY, 1970);
        Person p1 = new Person("joe", "bloggs", cal.getTime());
        
        Club fcbayern = new FootballClub("FC Bayern München");
        Member fbeckenbauer = new Member("franz", "beckenbauer", fcbayern, 1, cal.getTime());
        fcbayern.addMember(fbeckenbauer);
        
        VipMember krummenigge = new VipMember("Karlheiz", "Rummenigge", fcbayern, 2, cal.getTime());
        VipMember uhoeness = new VipMember("Uli", "Hoeness", fcbayern, 3, cal.getTime());
        VipMember cnerlinger = new VipMember("Christian", "Nerlinger", fcbayern, 4, cal.getTime());
        fcbayern.addVipMember(krummenigge);
        fcbayern.addVipMember(uhoeness);
        fcbayern.addVipMember(cnerlinger);

        ArrayList<VipMember> friends = new ArrayList<VipMember>();
        friends.add(uhoeness);
        friends.add(cnerlinger);
        krummenigge.setFriends(friends);
        
        Employee bschweinsteiger = new Employee("Bastian", "Schweinsteiger", cal.getTime(), 1);
        Employee mgomez = new Employee("Mario", "Gomez", cal.getTime(), 2);
        Employee hbadstuber = new Employee("Holger", "Badstuber", cal.getTime(), 3);

        ArrayList<Person> staff = new ArrayList<Person>();
        staff.add(bschweinsteiger);
        staff.add(mgomez);
        fcbayern.setStaff(staff);
        
    }
}
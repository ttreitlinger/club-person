package club;

public class Job {

    protected String title;
    protected String jobDescription;

    public Job(String title, String jobDescription) {
        this.title = title;
        this.jobDescription = jobDescription;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }
}

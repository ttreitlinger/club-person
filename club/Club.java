package club;


import java.util.ArrayList;
import java.util.List;

/**
 * Author: ttreitlinger
 * Date  : 14/11/2011
 * Time  : 21:37
 */
public class Club {

    protected String name;
    protected List<Member> members;
    protected List<Member> vipMembers;
    protected List<Person> staff;
    
    protected String interest;
    protected int memberCount;

    public Club(String name) {
        this.name = name;
        members = new ArrayList<Member>();
        vipMembers = new ArrayList<Member>();
        staff = new ArrayList<Person>();
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public void addMember(Member member) {
        if (! members.contains(member)){
            members.add(member);
            member.setClub(this);
        }
    }

    public void addVipMember(VipMember vipMember) {
        members.add(vipMember);
        makeVip(vipMember);         //TODO: unnecessary method call
    }

    public boolean makeVip(Member member){

        if (members.contains(member)) {
            vipMembers.add(member);
            return true;
        }
        return false;
    }

    public int getMemberShipNumber(Member member) {
        return member.membershipNumber;     //TODO: inappropriate intimacy: direct access to member internals
    }

    public int countMembers() {

        int size = 0;
        
        if (members.size() > 0){
            size = getMembers().get(0).getClub().getMembers().size();   //TODO: message chain
        }

        if (size > 0) {
            return size;
        } else {
            return 0;
        }
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount() {
        memberCount = countMembers();
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public int getVipCount() {

        int count = 0;
        for (Member m : members) {
            if (m instanceof VipMember) {
                count++;
            }
        }
        return count;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public List<Member> getVipMembers() {
        return vipMembers;
    }

    public void setVipMembers(List<Member> vipMembers) {
        this.vipMembers = vipMembers;
    }

    public List<Person> getStaff() {
        return staff;
    }

    public void setStaff(List<Person> staff) {
        this.staff = staff;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Club club = (Club) o;

        if (!name.equals(club.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}

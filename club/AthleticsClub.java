package club;

import java.util.List;

public class AthleticsClub extends Club {

    protected List<Facility> facilities;
    protected List<Sport> sports;
    protected int memberCount;

    public AthleticsClub(String name){
        super(name);
    }

    public List<Facility> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<Facility> facilities) {
        this.facilities = facilities;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public List<Sport> getSports() {
        return sports;
    }

    public void setSports(List<Sport> sports) {
        this.sports = sports;
    }


}

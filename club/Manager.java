package club;

import java.util.List;
import java.util.Date;

public class Manager extends Employee {

    private List<Employee> managedEmployees;

    
    public Manager(String lastName, String firstName, Date dateOfBirth, int staffNumber) {
        super(lastName, firstName, dateOfBirth, staffNumber);
    }


    public List<Employee> getManagedEmployees() {
        return managedEmployees;
    }
    
    public void setManagedEmployees(List<Employee> managedEmployees) {
        this.managedEmployees = managedEmployees;
    }
}

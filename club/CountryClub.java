package club;

import java.util.List;

public class CountryClub extends Club {

    protected List<Facility> facilities;
    protected int memberCount;

    public CountryClub(String name) {
        super(name);
    }

    public List<Facility> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<Facility> facilities) {
        this.facilities = facilities;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }
}

package club;

import java.util.Date;

public class Staff extends Person {

    protected Job job;
    protected int staffNumber;
    
    protected boolean permanent;
    protected Date dateJoined;
    protected Date dateLeft;

    public Staff(String lastName, String firstName, Date dateOfBirth, int staffNumber) {
        super(lastName, firstName, dateOfBirth);
        this.staffNumber = staffNumber;

    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public int getStaffNumber() {
        return staffNumber;
    }

    public void setStaffNumber(int staffNumber) {
        this.staffNumber = staffNumber;
    }

    public boolean isPermanent() {
        return permanent;
    }

    public void setPermanent(boolean permanent) {
        this.permanent = permanent;
    }

    public Date getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(Date dateJoined) {
        this.dateJoined = dateJoined;
    }

    public Date getDateLeft() {
        return dateLeft;
    }

    public void setDateLeft(Date dateLeft) {
        this.dateLeft = dateLeft;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Staff staff = (Staff) o;

        if (staffNumber != staff.staffNumber) return false;
        if (job != null ? !job.equals(staff.job) : staff.job != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = job != null ? job.hashCode() : 0;
        result = 31 * result + staffNumber;
        return result;
    }
}

package club;

import java.util.List;
import java.util.Date;

public class Volunteer extends Staff {

    protected Dedication dedication;
    protected List<Job> previousJobs;

    public Volunteer(String lastName, String firstName, Date dateOfBirth, int staffNumber) {
        super(lastName, firstName, dateOfBirth, staffNumber);
    }

    public Dedication getDedication() {
        return dedication;
    }

    public void setDedication(Dedication dedication) {
        this.dedication = dedication;
    }

    public List<Job> getPreviousJobs() {
        return previousJobs;
    }

    public void setPreviousJobs(List<Job> previousJobs) {
        this.previousJobs = previousJobs;
    }
}

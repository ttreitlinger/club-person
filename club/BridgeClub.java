package club;

public class BridgeClub extends SocialClub {

    protected String league;

    public BridgeClub(String name){
        super(name);
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

}


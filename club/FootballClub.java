package club;


public class FootballClub extends SportsClub {

    protected String league;

    public FootballClub(String name) {
        super(name);
    }


    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }
}

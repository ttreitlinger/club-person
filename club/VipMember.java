package club;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

/**
 * Author: ttreitlinger
 * Date  : 14/11/2011
 * Time  : 21:43
 */
public class VipMember extends Member {

    protected int vipPoints = 0;
    protected List<VipMember> friends;
    protected List<VipMember> enemies;


    public VipMember(String firstName, String lastName, Club club, int membershipNumber, Date dateJoined) {
        super(firstName, lastName, club, membershipNumber, dateJoined);
        friends = new ArrayList<VipMember>();
        enemies = new ArrayList<VipMember>();
    }

    public void addVipPoints(int newPoints) {
        vipPoints += newPoints;
    }

    public boolean isVip() {
        //TODO:
        return getClub().getVipMembers().contains(this);
    }


    public int getVipPoints() {
        return vipPoints;
    }

    public void setVipPoints(int vipPoints) {
        this.vipPoints = vipPoints;
    }

    public List<VipMember> getFriends() {
        return friends;
    }

    public void setFriends(List<VipMember> friends) {
        this.friends = friends;
    }

    public List<VipMember> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<VipMember> enemies) {
        this.enemies = enemies;
    }
}

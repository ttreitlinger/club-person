package club;

public class SocialClub extends Club {

    protected int memberCount;
    protected String interest;


    public SocialClub(String name) {
        super(name);
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }


}

package club;

import java.util.Date;

public class Employee extends Professional {

   protected Manager manager;
    
    public Employee(String lastName, String firstName, Date dateOfBirth, int staffNumber) {
        super(lastName, firstName, dateOfBirth, staffNumber);
    }
}

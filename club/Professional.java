package club;

import java.util.List;
import java.util.Date;

public class Professional extends Staff {

    protected int salary;
    protected Loyalty loyalty;
    protected List<Job> previousJobs;

    public Professional(String lastName, String firstName, Date dateOfBirth, int staffNumber) {
        super(lastName, firstName, dateOfBirth, staffNumber);
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Loyalty getLoyalty() {
        return loyalty;
    }

    public void setLoyalty(Loyalty loyalty) {
        this.loyalty = loyalty;
    }

    public List<Job> getPreviousJobs() {
        return previousJobs;
    }

    public void setPreviousJobs(List<Job> previousJobs) {
        this.previousJobs = previousJobs;
    }
}

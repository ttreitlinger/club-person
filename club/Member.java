package club;

import java.util.Date;

/**
 * Author: ttreitlinger
 * Date  : 14/11/2011
 * Time  : 21:37
 */
public class Member extends Person {

    protected Club club;
    public int membershipNumber;
    protected Date dateJoined;
    protected int loyaltyPoints = 0;
    
    public Member(String firstName, String lastName, Club club, int membershipNumber, Date dateJoined) {
        super(firstName, lastName);
        this.club = club;
        this.membershipNumber = membershipNumber;
        this.dateJoined = dateJoined;
    }

    /*
     * TODO:feature envy - more interested in Club data than Member/Person data;
     */
    public String getClubName() {
        return club.getName();
    }

    public String getClubInterest() {
        return club.getInterest();
    }

    public int getClubMemberCount() {
        return club.countMembers();
    }

    public int getClubVipMemberCount() {
        return club.getVipCount();
    }




    public void addLoyaltyPoints(int points) {
        loyaltyPoints += points;
    }


    /*
     * Getters/Setters
     */

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }


    public int getMembershipNumber() {
        return club.getMemberShipNumber(this);      //TODO: bad getter - gets result from club
    }

    public void setMembershipNumber(int membershipNumber) {
        this.membershipNumber = membershipNumber;
    }

    public int getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(int loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Member member = (Member) o;

        if (membershipNumber != member.membershipNumber) return false;
        if (!club.equals(member.club)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = club.hashCode();
        result = 31 * result + membershipNumber;
        return result;
    }
}

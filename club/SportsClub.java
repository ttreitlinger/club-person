package club;

import java.util.List;

public class SportsClub extends Club {

    protected List<Sport> sports;
    protected List<Facility> facilities;

    public SportsClub(String name) {
        super(name);
    }

    public List<Sport> getSports() {
        return sports;
    }

    public void setSports(List<Sport> sports) {
        this.sports = sports;
    }

    public List<Facility> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<Facility> facilities) {
        this.facilities = facilities;
    }
}
